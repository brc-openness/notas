# brc Openness

Esto surge de la necesidad de explicar alguna ideas y beneficios de Open Source / Agile / Kaizen (Mejora Continua) a personas que no vengan de sistemas o management o emprededurismo, donde se conocen estas filosofias


![sweet-spot](https://gitlab.com/brc-openness/notas/raw/master/sweet-spot.png)

# Caso Culturica

Joaquin, me comento que tiene la idea de replicar su proyecto en otras ciudades pero no tenia idea como o por donde empezar.

# Antecedentes

## TEDx
* ver - https://es.wikipedia.org/wiki/TED#TEDx y https://www.ted.com/participate/organize-a-local-tedx-event

Los TEDx tal vez son el ejemplo mas representativo de replicacion de un evento.
La licencia exclusiva de TED que puede ser obtenida por quien acuerde de antemano seguir ciertos principios ver [tedx-rules] (https://www.ted.com/participate/organize-a-local-tedx-event/before-you-start/tedx-rules)
Los TEDx no cobran entrada (y tampoco deberian cobrar a los expositores), se hacen sutentables a travez de sponsors.

## Pechakucha

* ver http://www.pechakucha.org/

Pechacucha cobra entradas (un parte del fee va a la organizacion)

## FuckUp Nights

* ver [Fuck Up Nights - Stories of business failure in more than 250 cities from 80 countries.] (https://fuckupnights.com/)

## Agile Open Camp
* ver [Argentina] (http://www.agileopencamp.com.ar) 
* ver [AgileOpen.camp] http://agileopen.camp/ 
* Comentario de Nicolas Paez [re-pensando-el-aoc](https://blog.nicopaez.com/2017/12/02/re-pensando-el-aoc/)
